<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaIntegerInterface interface file.
 * 
 * This represents a schema with integer values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaIntegerInterface extends JsonSchemaInterface
{

	/**
	 * Gets a default value for this schema.
	 * 
	 * @return ?int
	 */
	public function getDefault() : ?int;

	/**
	 * Gets an example value for this schema.
	 * 
	 * @return ?int
	 */
	public function getExample() : ?int;
	
	/**
	 * Gets the multiple of what it should be.
	 * 
	 * @return ?integer
	 */
	public function getMultipleOf() : ?int;
	
	/**
	 * Gets the minimum, inclusive, of the accepted range.
	 * 
	 * @return ?integer
	 */
	public function getMinimum() : ?int;
	
	/**
	 * Gets the minimum, exclusive, of the accepted range.
	 * 
	 * @return ?integer
	 */
	public function getExclusiveMinimum() : ?int;
	
	/**
	 * Gets the maximum, inclusive, of the accepted range.
	 * 
	 * @return ?integer
	 */
	public function getMaximum() : ?int;
	
	/**
	 * Gets the maximum, exclusive, of the accepted range.
	 * 
	 * @return ?integer
	 */
	public function getExclusiveMaximum() : ?int;
	
	/**
	 * Make this schema visited by the given visitor.
	 *
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
