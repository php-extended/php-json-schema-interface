<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use JsonException;
use PhpExtended\Reifier\ReificationThrowable;
use Stringable;

/**
 * JsonSchemaProviderInterface interface file.
 * 
 * This provides json schema objects from where they are stored.
 * 
 * @author Anastaszor
 */
interface JsonSchemaProviderInterface extends Stringable
{
	
	/**
	 * Provides the json schema object from the given file.
	 * 
	 * @param string $fileName
	 * @return JsonSchemaInterface
	 * @throws ReificationThrowable if data is not as expected
	 * @throws JsonException if the string cannot be decoded as json
	 * @throws InvalidArgumentException if the target file is not readable
	 */
	public function provideFromFile(string $fileName) : JsonSchemaInterface;
	
	/**
	 * Provides the json schema object from the given json string.
	 * 
	 * @param string $jsonString
	 * @return JsonSchemaInterface
	 * @throws ReificationThrowable if data is not as expected
	 * @throws JsonException if the string cannot be decoded as json
	 */
	public function provideFromString(string $jsonString) : JsonSchemaInterface;
	
	/**
	 * Provides the json schema object from the given json data.
	 * 
	 * @param array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $jsonData
	 * @return JsonSchemaInterface
	 * @throws ReificationThrowable if data is not as expected
	 */
	public function provideFromArray(array $jsonData) : JsonSchemaInterface;
	
}
