<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaStringInterface interface file.
 * 
 * This represents a schema with string values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaStringInterface extends JsonSchemaInterface
{

	/**
	 * Gets a default value for this schema.
	 * 
	 * @return ?string
	 */
	public function getDefault() : ?string;

	/**
	 * Gets an example value for this schema.
	 * 
	 * @return ?string
	 */
	public function getExample() : ?string;
	
	/**
	 * Gets the minimum length of the string.
	 * 
	 * @return ?integer
	 */
	public function getMinLength() : ?int;
	
	/**
	 * Gets the maximum length of the string.
	 * 
	 * @return ?integer
	 */
	public function getMaxLength() : ?int;
	
	/**
	 * Gets the regular expression pattern that must validate the string.
	 * 
	 * @return ?string
	 */
	public function getPattern() : ?string;
	
	/**
	 * Gets the format of the string.
	 * 
	 * @return ?string
	 */
	public function getFormat() : ?string;
	
	/**
	 * Make this schema visited by the given visitor.
	 * 
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
