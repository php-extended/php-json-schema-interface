<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaArrayInterface interface file.
 * 
 * This represents a schema with array values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaArrayInterface extends JsonSchemaInterface
{
	
	/**
	 * Gets the type of the items, for a single union-typed array.
	 * 
	 * @return ?JsonSchemaInterface
	 */
	public function getItems() : ?JsonSchemaInterface;
	
	/**
	 * Gets the types of the index is the type of the items.
	 * 
	 * @return array<integer, JsonSchemaInterface>
	 */
	public function getPrefixItems() : array;
	
	/**
	 * Gets the types that must at least one be present in the array.
	 * 
	 * @return array<integer, JsonSchemaInterface>
	 */
	public function getContains() : array;
	
	/**
	 * Gets the minimum number of times a contain type must be present.
	 * 
	 * @return ?integer
	 */
	public function getMinContains() : ?int;
	
	/**
	 * Gets the maximum number of times a contain type must be present.
	 * 
	 * @return ?integer
	 */
	public function getMaxContains() : ?int;
	
	/**
	 * Gets the minimum number of items that are present.
	 * 
	 * @return ?integer
	 */
	public function getMinItems() : ?int;
	
	/**
	 * Gets the maximum number of items that are present.
	 * 
	 * @return ?integer
	 */
	public function getMaxItems() : ?int;
	
	/**
	 * Gets whether unique items must be enforced.
	 * 
	 * @return ?boolean
	 */
	public function getUniqueItems() : ?bool;
	
	/**
	 * Make this schema visited by the given visitor.
	 *
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
