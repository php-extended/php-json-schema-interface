<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaObjectInterface interface file.
 * 
 * This represents a schema with object values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaObjectInterface extends JsonSchemaInterface
{
	
	/**
	 * The properties that are defined in this object.
	 * 
	 * @return array<string, JsonSchemaInterface>
	 */
	public function getProperties() : array;
	
	/**
	 * The properties that are specified using a naming pattern in this object.
	 * 
	 * @return array<string, JsonSchemaInterface>
	 */
	public function getPatternProperties() : array;
	
	/**
	 * The type of additional properties for this object, if any.
	 * 
	 * @return ?JsonSchemaInterface
	 */
	public function getAdditionalProperties() : ?JsonSchemaInterface;
	
	/**
	 * Whether this object allows for unevaluated properties.
	 * 
	 * @return ?boolean
	 */
	public function getUnevaluatedProperties() : ?bool;
	
	/**
	 * Gets the names of the properties that are required.
	 * 
	 * @return array<integer, string>
	 */
	public function getRequired() : array;
	
	/**
	 * Gets the names of the properties that can be validated against a schema.
	 * 
	 * @return ?JsonSchemaPropertyNameInterface
	 */
	public function getPropertyNames() : ?JsonSchemaPropertyNameInterface;
	
	/**
	 * Gets the minimum number of properties in the validated object.
	 *
	 * @return ?integer
	 */
	public function getMinProperties() : ?int;
	
	/**
	 * Gets the maximum number of properties in the validated object.
	 * 
	 * @return ?integer
	 */
	public function getMaxProperties() : ?int;
	
	/**
	 * Make this schema visited by the given visitor.
	 *
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
