<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use JsonSerializable;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * JsonSchemaInterface interface file.
 * 
 * This represents all the data that exists within a json schema.
 * 
 * @author Anastaszor
 * @see https://json-schema.org/understanding-json-schema/index.html
 */
interface JsonSchemaInterface extends JsonSerializable, Stringable
{
	
	/**
	 * The declared identifier of this schema.
	 * 
	 * @return ?UriInterface
	 */
	public function getId() : ?UriInterface;
	
	/**
	 * The declared schema specification that is used with this document.
	 * 
	 * @return ?UriInterface
	 */
	public function getSchema() : ?UriInterface;
	
	/**
	 * The anchor of this type, used for relative path in schemas.
	 * 
	 * @return ?string
	 */
	public function getAnchor() : ?string;
	
	/**
	 * Gets the path to an external type.
	 * 
	 * @return ?UriInterface
	 */
	public function getRef() : ?UriInterface;
	
	/**
	 * Gets the type of this type.
	 * 
	 * @return ?string
	 */
	public function getType() : ?string;
	
	/**
	 * Gets the title of this type.
	 * 
	 * @return ?string
	 */
	public function getTitle() : ?string;
	
	/**
	 * Gets the description of this type.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string;
	
	/**
	 * Gets the comment of this type.
	 * 
	 * @return ?string
	 */
	public function getComment() : ?string;
	
	/**
	 * Whether this type is deprecated.
	 * 
	 * @return ?boolean
	 */
	public function getDeprecated() : ?bool;

	/**
	 * Whether this type is nullable.
	 * 
	 * @return ?boolean
	 */
	public function getNullable() : ?bool;
	
	/**
	 * Whether this type is read only.
	 * 
	 * @return ?boolean
	 */
	public function getReadOnly() : ?bool;
	
	/**
	 * Whether this type is write only.
	 * 
	 * @return ?boolean
	 */
	public function getWriteOnly() : ?bool;
	
	/**
	 * Gets the definitions for subschemas.
	 * 
	 * @return array<string, JsonSchemaInterface>
	 */
	public function getDefs() : array;
	
	// {{{ Utility Methods
	
	/**
	 * Gets the schema that lies to the given path. Paths may begin with # or /
	 * to signify the current object, but a null or empty path will lead to
	 * a null result.
	 * 
	 * Also path that leads to properties that are not schemas return a null
	 * result.
	 * 
	 * In the path, forward slashes are designed as separators between
	 * properties of objects and will be carried as long as the chain of objects
	 * exists.
	 * 
	 * If $refs are present in objects traversed by the path, they are not
	 * resolved (meaning a part of a path may lead to a $ref, but the full path
	 * is not directly accessible without $ref resolution, and a null will be
	 * returned). 
	 * 
	 * @param ?string $path
	 * @return ?JsonSchemaInterface
	 */
	public function getFromPath(?string $path = null) : ?JsonSchemaInterface;
	
	/**
	 * Merges this json schema with another schema. The values of the other
	 * schema are preserved over the values of this schema in case of conflict.
	 * 
	 * @param ?JsonSchemaInterface $schema
	 * @return JsonSchemaInterface
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface;
	
	/**
	 * Make this schema visited by the given visitor.
	 *
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
	// }}} Utility
	
}
