<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaFloatInterface floaterface file.
 * 
 * This represents a schema with float values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaFloatInterface extends JsonSchemaInterface
{

	/**
	 * Gets a default value for this schema.
	 * 
	 * @return ?float
	 */
	public function getDefault() : ?float;

	/**
	 * Gets an example value for this schema.
	 * 
	 * @return ?float
	 */
	public function getExample() : ?float;
	
	/**
	 * Gets the minimum, inclusive, of the accepted range.
	 * 
	 * @return ?float
	 */
	public function getMinimum() : ?float;
	
	/**
	 * Gets the minimum, exclusive, of the accepted range.
	 * 
	 * @return ?float
	 */
	public function getExclusiveMinimum() : ?float;
	
	/**
	 * Gets the maximum, inclusive, of the accepted range.
	 * 
	 * @return ?float
	 */
	public function getMaximum() : ?float;
	
	/**
	 * Gets the maximum, exclusive, of the accepted range.
	 * 
	 * @return ?float
	 */
	public function getExclusiveMaximum() : ?float;
	
	/**
	 * Make this schema visited by the given visitor.
	 *
	 * @template T of null|integer|float|string|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
