<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaBooleanInterface interface file.
 * 
 * This represents a schema with bool values.
 * 
 * @author Anastaszor
 */
interface JsonSchemaBooleanInterface extends JsonSchemaInterface
{

	/**
	 * Gets a default value for this schema.
	 * 
	 * @return ?bool
	 */
	public function getDefault() : ?bool;

	/**
	 * Gets an example value for this schema.
	 * 
	 * @return ?bool
	 */
	public function getExample() : ?bool;
	
	/**
	 * Make this schema visited by the given visitor.
	 * 
	 * @template T of null|integer|float|bool|array|object
	 * @param JsonSchemaVisitorInterface<T> $visitor
	 * @return T
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor);
	
}
