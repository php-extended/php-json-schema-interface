<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use JsonSerializable;
use Stringable;

/**
 * JsonSchemaPropertyNameInterface class file.
 * 
 * This represents a schema to validate property names in a json object.
 * 
 * @author Anastaszor
 */
interface JsonSchemaPropertyNameInterface extends JsonSerializable, Stringable
{
	
	/**
	 * Gets the pattern that is used in this property schema.
	 * 
	 * @return string
	 */
	public function getPattern() : string;
	
}
