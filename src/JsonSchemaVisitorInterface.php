<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * JsonSchemaVisitorInterface interface file.
 * 
 * This represents a visitor that is able to visit any json shema class.
 * 
 * @author Anastaszor
 * @template T of null|integer|float|string|array|object
 */
interface JsonSchemaVisitorInterface extends Stringable
{
	
	/**
	 * Visits a schema to be determined.
	 *
	 * @param JsonSchemaInterface $schema
	 * @return T
	 */
	public function visit(JsonSchemaInterface $schema);
	
	/**
	 * Visits an untyped schema.
	 *
	 * @param JsonSchemaInterface $schema
	 * @return T
	 */
	public function visitSchemaRaw(JsonSchemaInterface $schema);
	
	/**
	 * Visits an array schema.
	 *
	 * @param JsonSchemaArrayInterface $schema
	 * @return T
	 */
	public function visitSchemaArray(JsonSchemaArrayInterface $schema);

	/**
     * Visits a boolean schema.
     *
     * @param JsonSchemaBooleanInterface $schema
     * @return T
	 */
	public function visitSchemaBoolean(JsonSchemaBooleanInterface $schema);
	
	/**
	 * Visits a float schema.
	 *
	 * @param JsonSchemaFloatInterface $schema
	 * @return T
	 */
	public function visitSchemaFloat(JsonSchemaFloatInterface $schema);
	
	/**
	 * Visits an integer schema.
	 *
	 * @param JsonSchemaIntegerInterface $schema
	 * @return T
	 */
	public function visitSchemaInteger(JsonSchemaIntegerInterface $schema);
	
	/**
	 * Visits an object schema.
	 *
	 * @param JsonSchemaObjectInterface $schema
	 * @return T
	 */
	public function visitSchemaObject(JsonSchemaObjectInterface $schema);
	
	/**
	 * Visits a string schema.
	 * 
	 * @param JsonSchemaStringInterface $schema
	 * @return T
	 */
	public function visitSchemaString(JsonSchemaStringInterface $schema);
	
}
