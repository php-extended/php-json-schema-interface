# php-extended/php-json-schema-interface

A library that specifies the schemas for parsing json-schema.org files

![coverage](https://gitlab.com/php-extended/php-json-schema-interface/badges/master/pipeline.svg?style=flat-square)

This is based on the [json-schema.org specification](https://json-schema.org/understanding-json-schema/index.html).


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-json-schema-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-json-schema-object`](https://gitlab.com/php-extended/php-json-schema-object).


## License

MIT (See [license file](LICENSE)).
